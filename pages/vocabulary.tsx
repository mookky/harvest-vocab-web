import { List, Typography, Space, Select, Row, Col } from 'antd';
import { IInitialProps } from '../interface'
import { ButtonStyled } from '../components/ui/button'
import { NextPage } from 'next'
import { LayoutTemplate } from '../components/ui/layout'
import styles from '../styles/Vocabulary.module.css'
import Cookies from 'next-cookies'
import { useEffect, useState } from 'react'
import { useLazyQuery, useQuery } from '@apollo/client'
import { VOCAB_QUERY, RANDOM_VOCAB } from '../documents/vocabulary'
import { IVocab } from '../interface/vocabulary'
import Link from 'next/link';

const { Text } = Typography
const { Option } = Select

const SelectVocab: NextPage<IInitialProps> = () => {
  const [myVocab, setMyVocab] = useState<IVocab[]>([])
  const [listVocab, setListVocabs] = useState<IVocab[]>([])

  const { data } = useQuery(VOCAB_QUERY, {
    fetchPolicy: 'network-only',
    variables: {
      input: {
        level: [],
        category: []
      }
    }
  })

  useEffect(() => {
    if(data) {
      setListVocabs(data?.vocabs)
      // console.log('listVocab:', listVocab)
    }
  }, [data])

  const onDeselectVocab = (vocab: IVocab) => {
    const index = myVocab.indexOf(vocab)
    if (index > -1) {
      myVocab.splice(index, 1)
    }
    setMyVocab(myVocab)
    console.log('deselect:', myVocab)
  }
  const onChange = (value: any) => {
    console.log(`selected ${value}`)
  }

  const onBlur = () => {
    console.log('blur')
  }

  const onFocus = () => {
    console.log('focus')
  }

  const onSearch = (val: any) => {
    console.log('search:', val)
  }

  useEffect(() =>{
    setMyVocab(myVocab)
  }, [myVocab])

  const MyVocabSelected = (vocab: IVocab[]) => {
    return vocab?.map((item) => 
        <List.Item onClick={() => onDeselectVocab(item)} className={styles.vocabItem}>
          <Space size={50}>
            <div className={styles.word}>{item.word}</div>
          </Space>
        </List.Item>
    )
  }

  return (
    <LayoutTemplate>
      <Text className={styles.header}>VOCABULARY</Text>
      <Space direction="horizontal" className={styles.filterBox}>
        <Select
          showSearch
          style={{ width: 200,  }}
          placeholder="Select a category"
          optionFilterProp="children"
          onChange={onChange}
          onFocus={onFocus}
          onBlur={onBlur}
          onSearch={onSearch}
          //        filterOption={(input, option) =>
          //     option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          //   }
        >
          <Option value="animal">animal</Option>
          <Option value="occupation">Noun</Option>
          <Option value="adj">Adj</Option>
        </Select>

        <ButtonStyled
          color="#000"
          padding="0"
          fontSize="18px"
          width="50px"
          height="30px"
          border="4rem"
          bgcolor="#FEB72B"
          hoverbgcolor="#F68500"
        >
          A1
        </ButtonStyled>
        <ButtonStyled
          color="#000"
          padding="0"
          fontSize="18px"
          width="50px"
          height="30px"
          border="4rem"
          bgcolor="#FEB72B"
          hoverbgcolor="#F68500"
        >
          A2
        </ButtonStyled>
        <ButtonStyled
          color="#000"
          padding="0"
          fontSize="18px"
          width="50px"
          height="30px"
          border="4rem"
          bgcolor="#FEB72B"
          hoverbgcolor="#F68500"
        >
          B1
        </ButtonStyled>
        <ButtonStyled
          color="#000"
          padding="0"
          fontSize="18px"
          width="50px"
          height="30px"
          border="4rem"
          bgcolor="#FEB72B"
          hoverbgcolor="#F68500"
        >
          B2
        </ButtonStyled>
        <ButtonStyled
          color="#000"
          padding="0"
          fontSize="18px"
          width="50px"
          height="30px"
          border="4rem"
          bgcolor="#FEB72B"
          hoverbgcolor="#F68500"
        >
          C1
        </ButtonStyled>
        <ButtonStyled
          color="#000"
          padding="0"
          fontSize="18px"
          width="50px"
          height="30px"
          border="4rem"
          bgcolor="#FEB72B"
          hoverbgcolor="#F68500"
        >
          C2
        </ButtonStyled>
        <Link href="/practice-mode">
          <ButtonStyled
            color="#000"
            padding="0"
            fontSize="18px"
            width="100px"
            height="30px"
            border="4rem"
            bgcolor="#FEB72B"
            margin="4rem"
            hoverbgcolor="#F68500"
          >
            Start
          </ButtonStyled>
        </Link>
      </Space>
      <div className={styles.mainbox}>
        <div className={styles.vocabBox}>
          <div className={styles.vocabBody}>
            <List
              size="small"
              header={
              <Row>
                <Col span={12} className={styles.boxHeader}>Word</Col>
                <Col span={12} style={{ justifyContent:"flex-end", display: "flex" }} className={styles.boxHeader}>Level</Col>
              </Row>
              }
              dataSource={listVocab}
              renderItem={(item) => (
                <List.Item onClick={() => setMyVocab([...myVocab, item])} className={styles.vocabItem}>
                  <div className={styles.word}>{item.word}</div>
                  <div>{item.level}</div>
                </List.Item>
              )}
            />
          </div>
        </div>
        <div className={styles.vocabChoosingbox}>
          <div className={styles.choosingBody}>
            <List
              size="small"
              header={
                <Row>
                  <Col span={24} style={{ justifyContent:"center", display: "flex" }} className={styles.boxHeader}>My Vocabs</Col>
                </Row>
              }
            >
              { MyVocabSelected(myVocab) }
              </List>
          </div>
        </div>
      </div>
    </LayoutTemplate>
  )
}

SelectVocab.getInitialProps = async (ctx: any): Promise<IInitialProps> => {
  const { req, res } = ctx
  const userAgent: string = req ? req.headers['user-agent'] || '' : navigator.userAgent
  const { user }: any = Cookies(ctx)

  return { userAgent, user }
}

export default SelectVocab
function ObjectRow(): { vocab: string; vategory: string } {
  throw new Error('Function not implemented.')
}
