import { IInitialProps } from '../interface'
import { LayoutTemplate } from '../components/ui/layout'
import { NextPage } from 'next'
import Cookies from 'next-cookies'
import styles from '../styles/ScorePage.module.css'
import { useRouter } from 'next/router'
import { IQuizScoreRouteQuery } from '../interface/quiz-score'

const ScorePage: NextPage<IInitialProps> = () => {
  const router = useRouter()
  const { correct, wrong, hint, skip } = router.query as IQuizScoreRouteQuery

  const score = parseInt(correct || '')
  const totalScore = score + parseInt(wrong || '')

  const percentage = totalScore ? (score / totalScore) * 100 : 0
  const isShow1stStar = percentage >= 50
  const isShow2ndStar = percentage >= 75
  const isShow3rdStar = percentage === 100

  return (
    <LayoutTemplate>
      <div className={styles.cardScore}>
        <div className={styles.scoreHeader}>
          <h3 className={styles.headerText}>I CAN DO IT</h3>
        </div>
        <div className={styles.scoreStars}>
          <img src={isShow1stStar ? '/static/images/star.png' : '/static/images/empty-star.png'} alt="*" className={styles.star} />
          <img src={isShow2ndStar ? '/static/images/star.png' : '/static/images/empty-star.png'} alt="*" className={styles.starCenter} />
          <img src={isShow3rdStar ? '/static/images/star.png' : '/static/images/empty-star.png'} alt="*" className={styles.star} />
        </div>
        <div className={styles.scoreContent}>
          <div className={styles.scoreRow}>
            <p className={styles.rowText}>CORRECT</p>
            <p className={styles.rowScore}>{correct}</p>
          </div>
          <div className={styles.scoreRow}>
            <p className={styles.rowText}>WRONG</p>
            <p className={styles.rowScore}>{wrong}</p>
          </div>
          <div className={styles.scoreRow}>
            <p className={styles.rowText}>HINT</p>
            <p className={styles.rowScore}>{hint}</p>
          </div>
        </div>
        <div className={styles.scoreButton}>
          <button className={styles.button}>TRY AGAIN</button>
          <button className={styles.button}>SAVE</button>
        </div>
      </div>
    </LayoutTemplate>
  )
}

ScorePage.getInitialProps = async (ctx: any): Promise<IInitialProps> => {
  const { req, res } = ctx
  const userAgent = req ? req.headers['user-agent'] || '' : navigator.userAgent
  const { user } = Cookies(ctx)
  console.log('score page')
  return { userAgent, user }
}

export default ScorePage
