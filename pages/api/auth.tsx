import axios, { AxiosRequestConfig } from 'axios'
import Config from '../../utils/config'

export type SigninType = {
  email: string
  password: string
}

export type RegisterType = {
  name: string
  email: string
  password: string
}

export const loginService = async ({ email, password }: SigninType) => {
  const options: AxiosRequestConfig = {
    method: 'POST',
    headers: { 'content-type': 'application/json' },
    data: { email, password },
    url: `${Config.BACKEND_BASE}/api/login`
  }
  const result = await axios(options)
  return result
}

export const registerService = async ({ email, password, name }: RegisterType) => {
  const options: AxiosRequestConfig = {
    method: 'POST',
    headers: { 'content-type': 'application/json' },
    data: { email, password, name },
    url: `${Config.BACKEND_BASE}/api/register`
  }
  const result = await axios(options)
  return result
}
