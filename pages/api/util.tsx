import Config from '../../utils/config'
import axios, { AxiosRequestConfig } from 'axios'

export const textToSpeechService = async (word: string) => {
  const options: AxiosRequestConfig = {
    method: 'GET',
    responseType: 'arraybuffer',
    headers: { 'content-type': 'application/json' },
    url: `${Config.BACKEND_BASE}/api/speech?text=${word || 'animals'}`
  }
  const result = await axios(options)
  return result
}
