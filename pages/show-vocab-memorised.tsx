import { List, Typography, Space, Select } from 'antd'
import { IInitialProps } from '../interface'
import { ButtonStyled } from '../components/ui/button'
import { NextPage } from 'next'
import { LayoutTemplate } from '../components/ui/layout'
import styles from '../styles/ShowVocabMemorised.module.css'
import Cookies from 'next-cookies'
import { useState } from 'react'
import { Row, Col } from 'antd'
const { Text, Link } = Typography
const { Option } = Select

function onChange(value: any) {
  console.log(`selected ${value}`)
}

interface IVocab {
  id: string
  word: string
  definition: string
  category: string
  level: string
}

const ShowVocabMemorised: NextPage<IInitialProps> = () => {
  const [myVocab, setMyVocab] = useState<IVocab[]>([])
  console.log(myVocab)

  const vocabList: IVocab[] = [
    { id: '1', word: 'asd', definition: 'n.', level: 'A1', category: 'animal' },
    { id: '2', word: 'asd', definition: 'n.', level: 'A1', category: 'animal' },
    { id: '3', word: 'asd', definition: 'n.', level: 'A1', category: 'animal' },
    { id: '4', word: 'asd', definition: 'n.', level: 'A1', category: 'animal' },
    { id: '5', word: 'asd', definition: 'n.', level: 'A1', category: 'animal' },
    { id: '6', word: 'asd', definition: 'n.', level: 'A1', category: 'animal' },
    { id: '7', word: 'asd', definition: 'n.', level: 'A1', category: 'animal' }
  ]

  // for (var i = 0; i < vocabList.length; i++) {
  //   vocabList.push(ObjectRow());
  // }
  // return tbody(vocabList);

  const onSelectVocab = (vocab: IVocab) => {
    myVocab.push(vocab)
    setMyVocab(myVocab)
  }

  const onDeselectVocab = (vocab: IVocab) => {
    const index = myVocab.indexOf(vocab)
    if (index > -1) {
      myVocab.splice(index, 1)
    }
  }
  const onChange = (value: any) => {
    console.log(`selected ${value}`)
  }

  const onBlur = () => {
    console.log('blur')
  }

  const onFocus = () => {
    console.log('focus')
  }

  const onSearch = (val: any) => {
    console.log('search:', val)
  }
  return (
    <LayoutTemplate>
      <Text className={styles.header}>VOCABULARY</Text>

      <div className={styles.mainbox}>
        <div className={styles.vocabBox}>
          <div className={styles.vocabBody}>
            <List
              size="small"
              header={<Text className={styles.boxHeader}>Word</Text>}
              dataSource={vocabList}
              renderItem={(item) => (
                <List.Item>
                  <Space size={300}>
                    <div className={styles.word}>{item.word}</div>
                    <div>{item.level}</div>
                  </Space>
                </List.Item>
              )}
            />
          </div>
        </div>
      </div>
      <div>
        <Link href="/select-mode">
          <ButtonStyled
            color="#000"
            padding="0"
            fontSize="18px"
            width="100px"
            height="30px"
            border="4rem"
            bgcolor="#FEB72B"
            margin="4rem"
            hoverbgcolor="#F68500"
          >
            Back
          </ButtonStyled>
        </Link>
      </div>
    </LayoutTemplate>
  )
}

ShowVocabMemorised.getInitialProps = async (ctx: any): Promise<IInitialProps> => {
  const { req, res } = ctx
  const userAgent: string = req ? req.headers['user-agent'] || '' : navigator.userAgent
  const { user }: any = Cookies(ctx)

  return { userAgent, user }
}

export default ShowVocabMemorised
function ObjectRow(): { vocab: string; vategory: string } {
  throw new Error('Function not implemented.')
}
