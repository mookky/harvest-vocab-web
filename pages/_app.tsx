import '../styles/globals.css'
import '../styles/font.css'
import 'antd/dist/antd.css'
import { useApollo } from '../lib/apollo'
import type { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client'
import GlobalStyle from '../styles/globalStyles'
import Head from 'next/head'
const MyApp = ({ Component, pageProps }: AppProps) => {
  const apolloClient = useApollo(pageProps)
  return (
    <>
      <GlobalStyle />
      <ApolloProvider client={apolloClient}>
        <Head>
          <title>Havest Vacab</title>
          <link rel="apple-touch-icon" sizes="180x180" href="/static/images/logo.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="/static/images/logo.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/static/images/logo.png" />
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Component {...pageProps} />
      </ApolloProvider>
    </>
  )
}

export default MyApp
