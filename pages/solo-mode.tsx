import { Space, Image, Typography, Layout, Row, Col, Input } from 'antd'
import React, { useEffect, useState } from 'react'
const { Text } = Typography
import { IInitialProps } from '../interface'
import { NextPage } from 'next'
import Cookies from 'next-cookies'
import listening from '../static/images/listening.png'
import audioSpeaker from '../static/images/audio-speaker.png'
import styles from '../styles/SoloMode.module.css'
import { LayoutTemplate } from '../components/ui/layout'
import { ButtonStyled } from '../components/ui/button'
import { useQuery } from '@apollo/client'
import { IVocab } from '../interface/vocabulary'
import { RANDOM_VOCAB } from '../documents/vocabulary'
import { FormItemStyled } from '../components/ui/form'
import { InputPlaceholder } from '../components/ui/input'
import { CountdownCircleTimer } from 'react-countdown-circle-timer'
import { textToSpeechService } from './api/util'

const { Content } = Layout
const vocabs: IVocab[] = [
  {
    id: '0',
    word: 'cat',
    definition: 'cat',
    level: 'string',
    category: 'string'
  },
  {
    id: '1',
    word: 'dog',
    definition: 'DDOG',
    level: 'string',
    category: 'string'
  },
  {
    id: '2',
    word: 'rabbit',
    definition: 'DDOG',
    level: 'string',
    category: 'string'
  },
  {
    id: '3',
    word: 'people',
    definition: 'PEOPLE',
    level: 'string',
    category: 'string'
  }
]
const SoloMode: NextPage<IInitialProps> = () => {
  const [randomVocab, setRandomVocabs] = useState<IVocab[]>([])
  const [readyPlay, setReadyPlay] = useState<boolean>(false)
  const [showHint, setShowHint] = useState<boolean>(false)
  const [visibleGame, setVisibleGame] = useState<boolean>(false)
  const [countSkip, setCountSkip] = useState<number>(0)
  const [countCorrect, setCountCorrect] = useState<number>(0)
  const [countHint, setCountHint] = useState<number>(0)
  const [indexVocab, setIndexVocab] = useState<number>(0)
  const [currentVocab, setCurrentVocab] = useState<IVocab>(vocabs[0])
  const [typingVocab, setTypingVocab] = useState<string>('')
  // const [typingVocab1, setTypingVocab1] = useState<string>('')
  const [correct, setCorrect] = useState<boolean>(false)

  const onListening = async (word: string) => {
    const result = await textToSpeechService(word)
    const blob = new Blob([result.data], { type: 'audio/mp3' })
    const url = URL.createObjectURL(blob)
    const audio = new Audio(url)
    audio.play()
  }

  const { data, loading, refetch } = useQuery(RANDOM_VOCAB, {
    fetchPolicy: 'network-only',
    variables: {
      size: 100
    }
  })

  useEffect(() => {
    if (data) {
      setRandomVocabs(data?.randomVocabs)
      setCurrentVocab(data?.randomVocabs?.[0])
    }
  }, [data, loading])

  const playGame = () => {
    setReadyPlay(true)
    setVisibleGame(true)
  }

  const onSkip = () => {
    nextQuiz()
    setCountSkip(countSkip + 1)
  }

  const nextQuiz = () => {
    setIndexVocab(indexVocab + 1)
    setCurrentVocab(randomVocab[indexVocab + 1])
    setShowHint(false)
    setCorrect(false)
    setTypingVocab('')
    console.table({
      indexVocab,
      typingVocab,
      currentVocab: currentVocab?.word
    })
  }
  const onPressEnter = (e: any) => {
    if (e.key === 'Enter') {
      onEnter()
    }
  }
  // useEffect(() => {
  //   console.log('typingVocab:', typingVocab)
  //   if (typingVocab === currentVocab?.word?.toUpperCase()) {
  //     setTypingVocab('')
  //     setCorrect(true)
  //     nextQuiz()
  //     setCountCorrect(countCorrect + 1)
  //     console.log('in if:', typingVocab)
  //   }
  // }, [typingVocab])

  const onEnter = () => {
    console.log(typingVocab === currentVocab?.word?.toUpperCase())
    if (typingVocab === currentVocab?.word?.toUpperCase()) {
      console.log(correct, 'typingVocab:', typingVocab, 'currentVocab:', currentVocab?.word?.toUpperCase())
      nextQuiz()
      setCountCorrect(countCorrect + 1)
    }
  }

  const onHint = () => {
    setCountHint(countHint + 1)
    setShowHint(true)
  }

  const onReset = () => {
    setReadyPlay(false)
    setShowHint(false)
    setVisibleGame(false)
    setCountSkip(0)
    setCountCorrect(0)
    setCountHint(0)
    setIndexVocab(0)
    setTypingVocab('')
    refetch()
  }

  const CountdownTime = () => {
    const timerProps = {
      isPlaying: true,
      size: 120,
      strokeWidth: 10
    }
    const minuteSeconds = 30
    const getTimeSeconds = (time: number) => (minuteSeconds - time) | 0
    const renderTime = (time: number) => {
      if (time === 0) {
        setVisibleGame(false)
      }
      return (
        <div className="time-wrapper">
          <div className="time" style={{ fontSize: '28px', textAlign: 'center' }}>
            {time}
          </div>
          <div>Seconds</div>
        </div>
      )
    }
    return (
      <CountdownCircleTimer
        {...timerProps}
        colors={[
          ['#004777', 0.33],
          ['#F7B801', 0.33],
          ['#A30000', 0.33]
        ]}
        duration={minuteSeconds}
        initialRemainingTime={minuteSeconds}>
        {({ elapsedTime }) => renderTime(getTimeSeconds(elapsedTime || 0))}
      </CountdownCircleTimer>
    )
  }

  return (
    <LayoutTemplate>
      <Space direction="vertical" align="center">
        <Content>
          <Image width={80} src={listening.src} preview={false} />
        </Content>
        {typingVocab == currentVocab?.word?.toUpperCase() && (
          <Text strong style={{ fontFamily: 'Ribeye', color: '#0e8f4e' }}>
            CORRECT!!
          </Text>
        )}
        {!readyPlay && (
          <>
            <div className={styles.content}>
              <p>
                Solo Mode for student who want to challenge yourself! <br />
                Press `Ready` to play Solo Mode
              </p>
            </div>
            <ButtonStyled htmlType="submit" color="#000" border="4rem" bgcolor="#FEB72B" hoverbgcolor="#F68500" onClick={() => playGame()}>
              Ready
            </ButtonStyled>
          </>
        )}
        {readyPlay && visibleGame && (
          <>
            <div className={styles.inputBox}>
              <Space direction="horizontal">
                <ButtonStyled
                  color="#000"
                  width="30px"
                  height="30px"
                  bgcolor="#FEB72B"
                  hoverbgcolor="#F68500"
                  padding="0"
                  onClick={() => onListening(currentVocab?.word)}>
                  <Image width={13} src={audioSpeaker.src} preview={false} className={styles.icon}></Image>
                </ButtonStyled>
                <FormItemStyled
                  data-placeholder={typingVocab}
                  name="name"
                  style={{ margin: '0', width: '30rem' }}
                  valuePropName={typingVocab}>
                  <Input
                    autoComplete="off"
                    key="word"
                    placeholder="VOCABULARY"
                    style={{ height: '50px', borderRadius: '53px' }}
                    value={typingVocab}
                    allowClear={true}
                    onKeyPress={onPressEnter}
                    onChange={(e) => setTypingVocab(e.target.value.toUpperCase())}
                  />
                </FormItemStyled>
              </Space>
            </div>

            <div className={styles.meanBox}>
              <div>
                <ButtonStyled
                  disabled={showHint}
                  key="hint"
                  color="#000"
                  bgcolor="#FEB72B"
                  hoverbgcolor="#F68500"
                  width="7rem"
                  margin="2rem"
                  fontSize="14px"
                  padding="0"
                  onClick={onHint}>
                  <Text strong style={{ fontFamily: 'Ribeye' }}>
                    HINT
                  </Text>
                </ButtonStyled>
                <ButtonStyled
                  key="skip"
                  color="#000"
                  bgcolor="#ed7278"
                  hoverbgcolor="#f3adb0"
                  width="7rem"
                  margin="2rem"
                  fontSize="14px"
                  padding="0"
                  onClick={onSkip}>
                  <Text strong style={{ fontFamily: 'Ribeye' }}>
                    SKIP
                  </Text>
                </ButtonStyled>
                <ButtonStyled
                  key="enter"
                  color="#000"
                  bgcolor="#52b745"
                  hoverbgcolor="#a0eb96"
                  width="7rem"
                  margin="2rem"
                  fontSize="14px"
                  padding="0"
                  onClick={onEnter}>
                  <Text strong style={{ fontFamily: 'Ribeye' }}>
                    ENTER
                  </Text>
                </ButtonStyled>
              </div>

              {showHint && (
                <div className={styles.hintBox}>
                  <Text strong>Definition</Text>
                  <p style={{ fontSize: '14px' }}>{currentVocab?.definition}</p>
                </div>
              )}
            </div>
            {CountdownTime()}
          </>
        )}
      </Space>
      <br />
      {readyPlay && (
        <div style={{ width: '30rem', padding: '0 0 0 9rem' }}>
          <Row>
            <Col span={12}>Correct:</Col>
            <Col span={12}>{countCorrect}</Col>
          </Row>
          <Row>
            <Col span={12}>Hint:</Col>
            <Col span={12}>{countHint}</Col>
          </Row>
          <Row>
            <Col span={12}>Skip:</Col>
            <Col span={12}>{countSkip}</Col>
          </Row>
        </div>
      )}
      {readyPlay && !visibleGame && (
        <ButtonStyled
          color="#000"
          bgcolor="#FEB72B"
          hoverbgcolor="#F68500"
          width="10rem"
          margin="2rem"
          fontSize="14px"
          padding="0"
          onClick={onReset}>
          <Text strong style={{ fontFamily: 'Ribeye' }}>
            PLAY AGAIN
          </Text>
        </ButtonStyled>
      )}
    </LayoutTemplate>
  )
}

SoloMode.getInitialProps = async (ctx: any): Promise<IInitialProps> => {
  const { req, res } = ctx
  const userAgent: string = req ? req.headers['user-agent'] || '' : navigator.userAgent
  const { user }: any = Cookies(ctx)
  if (!user) {
    res.writeHead(302, {
      Location: '/register'
    })
    res.end()
  }
  return { userAgent, user }
}

export default SoloMode
