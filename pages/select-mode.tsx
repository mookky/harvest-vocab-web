import { Space, Image } from 'antd'
import { ButtonStyled } from '../components/ui/button'
import { IInitialProps } from '../interface'
import { LayoutTemplate } from '../components/ui/layout'
import Link from 'next/link'
import { NextPage } from 'next'
import Cookies from 'next-cookies'
import styles from '../styles/SelectMode.module.css'

const SelectMode: NextPage<IInitialProps> = () => {
  return (
    <LayoutTemplate>
      <div className={styles.content}>
        <Space direction="vertical" align="center">
          <Image src="/static/images/logo.png" width="300" height="300" preview={false} />
          <Link href="/vocabulary">
            <ButtonStyled color="#000" border="4rem" bgcolor="#FEB72B" hoverbgcolor="#F68500">
              Practice Mode
            </ButtonStyled>
          </Link>
          <Link href="/solo-mode">
            <ButtonStyled color="#000" border="4rem" bgcolor="#FEB72B" hoverbgcolor="#F68500">
              Solo
            </ButtonStyled>
          </Link>

          <ButtonStyled color="#000" border="4rem" bgcolor="#FEB72B" hoverbgcolor="#F68500">
            Multiple Player
          </ButtonStyled>
        </Space>
      </div>
    </LayoutTemplate>
  )
}

SelectMode.getInitialProps = async (ctx: any): Promise<IInitialProps> => {
  const { req, res } = ctx
  const userAgent: string = req ? req.headers['user-agent'] || '' : navigator.userAgent
  const { user }: any = Cookies(ctx)

  return { userAgent, user }
}

export default SelectMode
