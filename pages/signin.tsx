import { Form, notification } from 'antd'
import { handleError } from '../lib/common'
import { NextPage } from 'next'
import { useState } from 'react'
import { IInitialProps } from '../interface'
import Cookies from 'next-cookies'
import { LayoutTemplate } from '../components/ui/layout'
import { loginService, SigninType } from './api/auth'
import { InputPlaceholder } from '../components/ui/input'
import { ButtonStyled } from '../components/ui/button'
import Image from 'next/image'
import Router from 'next/router'
import { FormItemStyled } from '../components/ui/form'
import { LockOutlined, MailOutlined } from '@ant-design/icons'

const Signin: NextPage<IInitialProps> = () => {
  const [error, setError] = useState('')
  const onFinish = async (values: SigninType) => {
    const result: any = await loginService(values).catch((err: any) => {
      const error = handleError(err)
      notification.error({
        message: 'ERROR!',
        description: error
      })
      setError(error)
      return
    })
    if (result) {
      document.cookie = `user=${JSON.stringify({ email: values.email })};path=/`
      document.cookie = `token=${result?.data?.data}; path=/`
      notification.success({
        message: 'SUCCESS!',
        description: 'Login is successful.'
      })
      Router.push({ pathname: '/select-mode' })
      return
    }
  }

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo)
  }

  return (
    <LayoutTemplate title="Signin">
      <div>
        <Image src="/static/images/logo.png" width="300" height="300" />
      </div>
      <div>
        <Form name="basic" initialValues={{ remember: true }} onFinish={onFinish} onFinishFailed={onFinishFailed} autoComplete="off">
          <FormItemStyled data-placeholder="EMAIL" name="email" rules={[{ required: true, message: 'Please input your email!' }]}>
            <InputPlaceholder prefix={<MailOutlined className="site-form-item-icon" />} placeholder="EMAIL" />
          </FormItemStyled>
          <FormItemStyled name="password" rules={[{ required: true, message: 'Please input your password!' }]}>
            <InputPlaceholder prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="PASSSWORD" />
          </FormItemStyled>
          <FormItemStyled wrapperCol={{ offset: 6, span: 16 }}>
            <ButtonStyled htmlType="submit" color="#000" border="4rem" margin="3rem 0" bgcolor="#FEB72B" hoverbgcolor="#F68500">
              SIGNIN
            </ButtonStyled>
          </FormItemStyled>
        </Form>
      </div>
    </LayoutTemplate>
  )
}

Signin.getInitialProps = async (ctx: any): Promise<IInitialProps> => {
  const { req, res } = ctx
  const userAgent: string = req ? req.headers['user-agent'] || '' : navigator.userAgent
  const { user, token }: any = Cookies(ctx)
  console.log({ user, token })
  return { userAgent, user }
}

export default Signin
