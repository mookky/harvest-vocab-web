import { Button, Modal, Input, Space, Image, Typography } from 'antd'
import React, { useState } from 'react'
const { Text } = Typography
import { IInitialProps } from '../interface'
import { NextPage } from 'next'
import Cookies from 'next-cookies'
import listening from '../static/images/listening.png'
import audioSpeaker from '../static/images/audio-speaker.png'
import successChild from '../static/images/successChild.png'
import styles from '../styles/PracticeModeQuiz.module.css'
import { LayoutTemplate } from '../components/ui/layout'
import { ButtonStyled } from '../components/ui/button'
import Link from 'next/link'

const PracticeModeQuiz: NextPage<IInitialProps> = () => {
  const [visible, setVisible] = useState(false)
  const [showHint, setShowHint] = useState(false)
  return (
    <LayoutTemplate title="Quiz">
      <div className={styles.content}>
        <Space direction="vertical" align="center">
          <Image width={80} src={listening.src} preview={false} />

          <div className={styles.inputBox}>
            <Space direction="horizontal">
              <ButtonStyled color="#000" width="30px" height="30px" bgcolor="#FEB72B" hoverbgcolor="#F68500" padding="0">
                <Image width={13} src={audioSpeaker.src} preview={false} className={styles.icon}></Image>
              </ButtonStyled>
              <Input placeholder="WORD FORM SELECTED" />
            </Space>
          </div>

          <div className={styles.hintBox}>
            <ButtonStyled
              color="#000"
              fontSize="14px"
              width="50px"
              height="30px"
              bgcolor="#FEB72B"
              hoverbgcolor="#F68500"
              padding="0"
              onClick={() => setShowHint(!showHint)}
            >
              Hint
            </ButtonStyled>

            {showHint && (
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque
                ipsa quae ab illo inventore veritatis
              </p>
            )}
          </div>
        </Space>

        <Button type="primary" onClick={() => setVisible(true)}>
          success
        </Button>
        <Modal centered visible={visible} onOk={() => setVisible(false)} onCancel={() => setVisible(false)} width={500}>
          <Space className={styles.modalItem} direction="vertical" align="center">
            <Text className={styles.ModalHeader}>I can do it.</Text>
            <Image width={100} src={successChild.src} preview={false} />
            <Link href="/practice-mode-quiz">
              <Button type="primary" shape="round">
                Try Again
              </Button>
            </Link>
            <Link href="/select-vocab-memorised">
              <Button type="primary" shape="round">
                Select vocab
              </Button>
            </Link>
            <Link href="/select-mode">
              <Button type="primary" shape="round">
                Quit
              </Button>
            </Link>
          </Space>
        </Modal>
      </div>
    </LayoutTemplate>
  )
}

PracticeModeQuiz.getInitialProps = async (ctx: any): Promise<IInitialProps> => {
  const { req, res } = ctx
  const userAgent: string = req ? req.headers['user-agent'] || '' : navigator.userAgent
  const { user }: any = Cookies(ctx)

  return { userAgent, user }
}

export default PracticeModeQuiz
