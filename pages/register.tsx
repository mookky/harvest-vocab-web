import { Form, notification } from 'antd'
import { handleError } from '../lib/common'
import { NextPage } from 'next'
import { useState } from 'react'
import { IInitialProps } from '../interface'
import Cookies from 'next-cookies'
import { LayoutTemplate } from '../components/ui/layout'
import Image from 'next/image'
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons'
import { InputPlaceholder } from '../components/ui/input'
import { ButtonStyled } from '../components/ui/button'
import { registerService, RegisterType } from './api/auth'
import Router from 'next/router'
import { FormItemStyled } from '../components/ui/form'
const Register: NextPage<IInitialProps> = () => {
  const [error, setError] = useState('')
  const onFinish = async (values: RegisterType) => {
    const result: any = await registerService(values).catch((err: any) => {
      const error = handleError(err)
      notification.error({
        message: 'ERROR!',
        description: error
      })
      setError(error)
      return
    })
    if (result?.data) {
      document.cookie = `user=${JSON.stringify({ email: values.email, name: values.name })};path=/`
      document.cookie = `token=${result?.data?.data}; path=/`
      notification.success({
        message: 'SUCCESS!',
        description: 'Register is successful.'
      })
      Router.push({ pathname: '/select-mode' })
    }
  }

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo)
  }

  return (
    <LayoutTemplate title="Havest Register">
      <div>
        <Image src="/static/images/logo.png" width="300" height="300" />
      </div>
      <div>
        <Form name="basic" onFinish={onFinish} onFinishFailed={onFinishFailed} autoComplete="off">
          <FormItemStyled data-placeholder="NAME" name="name" rules={[{ required: true, message: 'Please input your email!' }]}>
            <InputPlaceholder prefix={<UserOutlined className="site-form-item-icon" />} placeholder="NAME" />
          </FormItemStyled>
          <FormItemStyled data-placeholder="EMAIL" name="email" rules={[{ required: true, message: 'Please input your email!' }]}>
            <InputPlaceholder prefix={<MailOutlined className="site-form-item-icon" />} placeholder="EMAIL" />
          </FormItemStyled>
          <FormItemStyled name="password" rules={[{ required: true, message: 'Please input your password!' }]}>
            <InputPlaceholder prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="PASSSWORD" />
          </FormItemStyled>
          <FormItemStyled wrapperCol={{ offset: 6, span: 16 }}>
            <ButtonStyled htmlType="submit" color="#000" border="4rem" margin="3rem 0" bgcolor="#FEB72B" hoverbgcolor="#F68500">
              PLAY
            </ButtonStyled>
          </FormItemStyled>
        </Form>
      </div>
    </LayoutTemplate>
  )
}

Register.getInitialProps = async (ctx: any): Promise<IInitialProps> => {
  const { req, res } = ctx
  const userAgent: string = req ? req.headers['user-agent'] || '' : navigator.userAgent
  const { user }: any = Cookies(ctx)
  console.log({ user })
  return { userAgent, user }
}

export default Register
