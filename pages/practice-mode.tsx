import { Button, Modal, Input, Space, Image, Typography, Layout, Form } from 'antd'
import React, { useEffect, useState } from 'react'
const { Text } = Typography
import { IVocab } from '../interface/vocabulary'
import { IInitialProps } from '../interface'
import { NextPage } from 'next'
import Cookies from 'next-cookies'
import listening from '../static/images/listening.png'
import audioSpeaker from '../static/images/audio-speaker.png'
import successChild from '../static/images/successChild.png'
import styles from '../styles/PracticeMode.module.css'
import { LayoutTemplate } from '../components/ui/layout'
import { ButtonStyled } from '../components/ui/button'
import Link from 'next/link'
import { useQuery } from '@apollo/client'
import { USER_QUERY } from '../documents/user'
import { textToSpeechService } from './api/util'
import { RANDOM_VOCAB } from '../documents/vocabulary'
import { FormItemStyled } from '../components/ui/form'
import { InputPlaceholder } from '../components/ui/input'

const { Content } = Layout

const vocabs: IVocab[] = [
  {
    id: '0',
    word: 'cat',
    definition: 'cat',
    level: 'string',
    category: 'string'
  },
  {
    id: '1',
    word: 'dog',
    definition: 'DDOG',
    level: 'string',
    category: 'string'
  },
  {
    id: '2',
    word: 'rabbit',
    definition: 'DDOG',
    level: 'string',
    category: 'string'
  },
  {
    id: '3',
    word: 'people',
    definition: 'PEOPLE',
    level: 'string',
    category: 'string'
  }
]

const PracticeMode: NextPage<IInitialProps> = () => {
  const [randomVocab, setRandomVocabs] = useState<IVocab[]>([])
  const [readyPlay, setReadyPlay] = useState<boolean>(false)
  const [visibleGame, setVisibleGame] = useState<boolean>(false)
  const [indexVocab, setIndexVocab] = useState<number>(0)
  const [currentVocab, setCurrentVocab] = useState<IVocab>(vocabs[0])
  const [typingVocab, setTypingVocab] = useState<string>('')
  const [correct, setCorrect] = useState<boolean>(false)
  const onListening = async (word: string) => {
    const result = await textToSpeechService(word)
    const blob = new Blob([result.data], { type: 'audio/mp3' })
    const url = URL.createObjectURL(blob)
    const audio = new Audio(url)
    audio.play()
  }

  const { data, loading } = useQuery(RANDOM_VOCAB, {
    // fetchPolicy: 'network-only',
    variables: {
      size: 100
    }
  })

  useEffect(() => {
    if (data) {
      setRandomVocabs(data?.randomVocabs)
      setCurrentVocab(data?.randomVocabs?.[0])
    }
  }, [data, loading])
  const playGame = () => {
    setReadyPlay(true)
    setVisibleGame(true)
  }

  const nextQuiz = () => {
    setIndexVocab(indexVocab + 1)
    setCurrentVocab(randomVocab[indexVocab + 1])
    setCorrect(false)
    setTypingVocab('')
    if (indexVocab == 3) {
      setVisibleGame(false)
    }
    console.table({
      indexVocab,
      currentVocab: currentVocab?.word
    })
  }

  const onEnter = () => {
    console.log('typingVocab:', typingVocab)
    console.log('currentVocab:', currentVocab?.word?.toLocaleUpperCase())
    if (typingVocab === currentVocab?.word?.toLocaleUpperCase()) {
      console.log(correct, 'typingVocab:', typingVocab, 'currentVocab:', currentVocab?.word?.toLocaleUpperCase())
      nextQuiz()
    }
  }
  const onPressEnter = (e: any) => {
    if (e.key === 'Enter') {
      onEnter()
    }
  }

  return (
    <LayoutTemplate>
      <div className={styles.content}>
        <Space direction="vertical" align="center">
          {/* { indexVocab } { currentVocab?.word } */}
          {correct && (
            <Text strong style={{ fontFamily: 'Ribeye', color: '#0e8f4e' }}>
              CORRECT!!
            </Text>
          )}
          {!readyPlay && (
            <>
              <Content>
                <Image width={80} src={listening.src} preview={false} />
              </Content>
              <p>Press `Ready` to practice</p>

              <ButtonStyled
                htmlType="submit"
                color="#000"
                border="4rem"
                bgcolor="#FEB72B"
                hoverbgcolor="#F68500"
                onClick={() => playGame()}>
                Ready
              </ButtonStyled>
            </>
          )}
          {readyPlay && visibleGame && (
            <>
              <div>
                <Space direction="vertical" size={50} className={styles.inputBox}>
                  <ButtonStyled
                    color="#000"
                    width="80px"
                    height="80px"
                    bgcolor="#FEB72B"
                    hoverbgcolor="#F68500"
                    padding="0"
                    onClick={() => onListening(currentVocab?.word)}>
                    <Image width={25} src={audioSpeaker.src} preview={false} className={styles.icon}></Image>
                  </ButtonStyled>
                  <FormItemStyled
                    data-placeholder="VOCABULARY"
                    name="name"
                    style={{ margin: '0', width: '30rem' }}
                    valuePropName={typingVocab}>
                    <InputPlaceholder
                      autoComplete="off"
                      key="word"
                      placeholder={currentVocab?.word}
                      style={{ height: '50px', borderRadius: '53px' }}
                      value={typingVocab}
                      onKeyDown={onPressEnter}
                      onChange={(e) => setTypingVocab(e.target.value.toLocaleUpperCase())}
                    />
                  </FormItemStyled>

                  {/* meaning */}
                  <div className={styles.meanBox}>
                    <div>
                      <Text strong>Meaning</Text>
                    </div>

                    <p style={{ fontSize: '14px' }}>{currentVocab?.definition}</p>
                  </div>

                  <ButtonStyled
                    key="enter"
                    color="#000"
                    bgcolor="#52b745"
                    hoverbgcolor="#a0eb96"
                    width="7rem"
                    margin="1rem"
                    fontSize="14px"
                    padding="0"
                    onClick={onEnter}>
                    <Text strong style={{ fontFamily: 'Ribeye' }}>
                      ENTER
                    </Text>
                  </ButtonStyled>
                </Space>
              </div>
            </>
          )}
        </Space>

        {/* <Button type="primary" onClick={() => setVisibleGame(true)}>
          success
        </Button> */}

        {readyPlay && !visibleGame && (
          <Space className={styles.modalItem} direction="vertical" align="center">
            <Text className={styles.ModalHeader}>I can do it.</Text>
            <Image width={100} src={successChild.src} preview={false} />
            <Button type="primary" shape="round">
              TRY AGAIN
            </Button>
            <Link href="/select-vocab-memorised">
              <Button type="primary" shape="round">
                COLLECT MY VOCAB
              </Button>
            </Link>
          </Space>
        )}
      </div>
    </LayoutTemplate>
  )
}

PracticeMode.getInitialProps = async (ctx: any): Promise<IInitialProps> => {
  const { req, res } = ctx
  const userAgent: string = req ? req.headers['user-agent'] || '' : navigator.userAgent
  const { user }: any = Cookies(ctx)

  return { userAgent, user }
}

export default PracticeMode
