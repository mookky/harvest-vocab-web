export interface IVocab {
  id: string
  word: string
  definition: string
  level: string
  category: string
}
