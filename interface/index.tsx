export interface IError {
  graphQLErrors: any
  networkError: any
  message: string
}

export interface IInitialProps {
  userAgent: string
  user: any
  form?: any
  pathname?: string
  query?: any
  token?: any
  namespacesRequired?: [string]
}
