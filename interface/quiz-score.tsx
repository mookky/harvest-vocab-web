export interface IQuizScoreRouteQuery {
  correct?: string
  wrong?: string
  hint?: string
  skip?: string
}
