import { gql } from '@apollo/client'

export const FVOCAB = gql`
  fragment Vocabulary on Vocabulary {
    id
    word
    definition
    level
    category
  }
`
export const FilterVocab = gql`
  input FilterVocab {
    category: [String!]!
    level: [String!]!
  }
`

export const VOCAB_QUERY = gql`
  query vocabs($input: FilterVocab!) {
    vocabs(input: $input) {
      ...Vocabulary
    }
  }
  ${FVOCAB}
`

export const RANDOM_VOCAB = gql`
  query randomVocabs($size: Int!) {
    randomVocabs(size: $size) {
      ...Vocabulary
    }
  }
  ${FVOCAB}
`
