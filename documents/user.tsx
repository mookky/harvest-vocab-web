import { gql } from '@apollo/client'

export const USER_QUERY = gql`
  query user {
    user {
      id
      email
      name
    }
  }
`

export const USERBYID_QUERY = gql`
  query userById($id: String!) {
    userById(id: $id) {
      id
      email
      role
    }
  }
`
