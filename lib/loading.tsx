let loadingEl: any
const style: string = 'position: fixed; width: 100%;'
const addLoading = () => {
  if (process.browser) {
    loadingEl = document.createElement('div')
    loadingEl.setAttribute('id', 'loading-bar')
    loadingEl.setAttribute('class', 'loading-bar')
    loadingEl.setAttribute('style', `display: block; ${style}`)

    document.body.prepend(loadingEl)
  }
}

export const startLoading = () => {
  if (process.browser) {
    loadingEl.style.display = 'block'
  }
}

export const closeLoading = () => {
  if (process.browser) {
    setTimeout(() => {
      loadingEl.style.display = 'none'
    }, 1000)
  }
}

export const Loading = () => {
  // Loading bar
  addLoading()
  startLoading()

  setTimeout(() => {
    closeLoading()
  }, 500)
}
