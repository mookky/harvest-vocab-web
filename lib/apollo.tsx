import { useMemo } from 'react'
import { ApolloClient, ApolloLink, from, HttpLink, InMemoryCache, NormalizedCacheObject } from '@apollo/client'
import { concatPagination } from '@apollo/client/utilities'
import merge from 'deepmerge'
import { isEqual } from 'lodash'
import Cookies from 'next-cookies'
import Config from '../utils/config'

let apolloClient: ApolloClient<NormalizedCacheObject>

const createApolloClient = () => {
  const httpLink: any = new HttpLink({
    uri: `${Config.BACKEND_BASE}/query`,
    credentials: 'same-origin',
    fetch
  })
  const authLink = new ApolloLink((operation, forward): any => {
    const { token } = Cookies(operation.getContext())

    // add the authorization to the headers
    setHeader(operation, token || '')

    return forward(operation)
  })

  const afterLink = new ApolloLink((operation, forward): any => {
    return forward(operation)
  })

  const setHeader = (operation: any, token: string) => {
    operation.setContext(({ headers = {} }: any): any => {
      return {
        headers: {
          ...headers,
          Authorization: token ? token : ''
        }
      }
    })

    return operation
  }
  let link: any = from([authLink, afterLink, httpLink])

  // Check out https://github.com/zeit/next.js/pull/4611 if you want to use the AWSAppSyncClient
  const client = new ApolloClient({
    ssrMode: typeof window === 'undefined', // Disables forceFetch on the server (so queries are only run once)
    link,
    cache: new InMemoryCache({
      typePolicies: {
        Query: {
          fields: {
            allPosts: concatPagination()
          }
        }
      }
    }).restore({})
  })

  return client
}

export const initializeApollo = (initialState = null) => {
  const _apolloClient = apolloClient ?? createApolloClient()

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // get hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract()

    // Merge the existing cache into data passed from getStaticProps/getServerSideProps
    const data = merge(initialState, existingCache, {
      // combine arrays using object equality (like in sets)
      arrayMerge: (destinationArray, sourceArray) => [
        ...sourceArray,
        ...destinationArray.filter((d) => sourceArray.every((s) => !isEqual(d, s)))
      ]
    })

    // Restore the cache with the merged data
    _apolloClient.cache.restore(data)
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') return _apolloClient
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient

  return _apolloClient
}

export const useApollo = (initialState: null | undefined) => {
  const store = useMemo(() => initializeApollo(initialState), [initialState])
  return store
}
