import { IError } from '../interface'

export const handleError = (mutationError: IError): string => {
  return mutationError?.message.replace(/^.+:\s/, '')
}
