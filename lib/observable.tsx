import { Observable } from 'apollo-link'

export default (promise: Promise<any>): Observable<any> =>
  new Observable((subscriber: any) => {
    promise.then(
      (value: any) => {
        if (subscriber.closed) return
        subscriber.next(value)
        subscriber.complete()
      },
      (err: any) => subscriber.error(err)
    )
    return subscriber // this line can removed, as per next comment
  })
