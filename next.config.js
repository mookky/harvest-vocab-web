/** @type {import('next').NextConfig} */
const withAntdLess = require('next-plugin-antd-less')
const path = require('path')

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true'
})

module.exports = withBundleAnalyzer(
  withAntdLess({
    cssLoaderOptions: {},
    // Other Config Here...
    // async headers() {
    //   return [
    //     {
    //       // Apply these headers to all routes in your application.
    //       source: '/(.*)',
    //       headers: []
    //     }
    //   ]
    // },
    webpack(config) {
      return config
    },
    optimizeFonts: true,
    reactStrictMode: true,
    sassOptions: {
      includePaths: [path.join(__dirname, 'styles')]
    }
  })
)
