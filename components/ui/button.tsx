import styled from 'styled-components'
import { Button } from 'antd'
import { ButtonProps } from 'antd/lib/button/button'

export const ButtonStyled: any = styled(Button)<{
  bgcolor?: string
  color?: string
  hoverbgcolor?: string
  hovercolor?: string
  border?: string
  margin?: string
  padding?: string
  fontSize?: string
  width?: string
  height?: string
}>`
  font-family: 'Ribeye';
  font-size: ${(props) => props.fontSize || '18px'};
  background-color: ${(props) => props.bgcolor || '#404040'};
  border-color: ${(props) => props.bgcolor || '#404040'};
  color: ${(props) => props.color || '#FFF'};
  border-radius: ${(props) => props.border || '53px'};
  margin: ${(props) => props.margin || 0};
  padding: ${(props) => props.padding || '0 1em'};
  line-height: 28px;
  width: ${(props) => props.width || '15rem'};
  height: ${(props) => props.height || '4rem'};
  &:hover,
  &:focus {
    background-color: ${(props) => props.hoverbgcolor || '#4f4f4f'};
    border-color: ${(props) => props.hoverbgcolor || '#4f4f4f'};
    color: ${(props) => props.hovercolor || '#FFF'};
  }
  .ant-btn[disabled] {
    border-color: #f2f2f2;
  }
`
export const ButtonStyledBorder: any = styled(Button)<{
  bgcolor?: string
  color?: string
  hoverbgcolor?: string
  hovercolor?: string
  bordercolor?: string
  hoverbordercolor?: string
}>`
  background-color: ${(props) => props.bgcolor || '#404040'};
  border-color: ${(props) => props.bordercolor || '#404040'};
  color: ${(props) => props.color || '#FFF'};
  line-height: 22px;
  border-radius: 5px;
  &:hover,
  &:focus {
    background-color: ${(props) => props.hoverbgcolor || '#4f4f4f'};
    border-color: ${(props) => props.hoverbordercolor || '#4f4f4f'};
    color: ${(props) => props.hovercolor || '#FFF'};
  }
`

interface ButtonStyledTypeProps extends ButtonProps {
  btype: 'danger' | 'success' | 'warning' | 'primary'
}

const typeBGColor = {
  success: '#0F9048',
  danger: '#EF4030',
  warning: '#0F9048',
  primary: '#FEB72B'
}

const hoverBGColor = {
  success: '#1d854b',
  danger: '#de3b2c',
  warning: '#1d854b',
  primary: '#B17600'
}

export const ButtonStyledType = ({ children, btype, ...rest }: ButtonStyledTypeProps) => (
  <ButtonStyled bgcolor={typeBGColor[btype]} hoverbgcolor={hoverBGColor[btype]} {...rest} border="5px">
    {children}
  </ButtonStyled>
)
