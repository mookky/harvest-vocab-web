import { Form, Layout, Col, Row } from 'antd'
import styled from 'styled-components'
import Head from 'next/head'

export const LayoutTemplate = (props: any) => {
  return (
    <>
      <Head>
        <title>{props.title || 'Harvest Vocab'}</title>
      </Head>
      <div className="container">{props.children}</div>
    </>
  )
}

export interface IDiv {
  display?: string | undefined
  width?: string | undefined
  margin?: string | undefined
  padding?: string | undefined
  justifyContent?: string | undefined
}

export const Div = styled.div`
  ${(props: IDiv) => {
    return {
      display: props.display || 'block',
      width: props.width,
      margin: props.margin,
      padding: props.padding,
      'justify-content': props.justifyContent
    }
  }}
`

export const FormAntd = styled(Form)`
  ${(props: IDiv) => {
    return {
      width: props.width
    }
  }}
`
