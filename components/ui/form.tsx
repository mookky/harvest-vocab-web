import styled from 'styled-components'
import { Form } from 'antd'

export const FormItemStyled = styled(Form.Item)`
  .ant-input-affix-wrapper {
    border-radius: 53px;
    padding: 15px;
    min-width: 30rem;
    border-color: #feb72b;
  }
  .ant-input-affix-wrapper-focused {
    box-shadow: unset;
  }
  .ant-input[disabled],
  .ant-input-disabled,
  .ant-input-affix-wrapper-disabled {
    background-color: transparent !important;
    border-bottom: none;
    color: #000000;
    border-color: #feb72b;
  }
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: #feb72b;
  }
`
