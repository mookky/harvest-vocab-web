import styled from 'styled-components'
import { Input } from 'antd'

export const InputPlaceholder = styled(Input)`
  input.ant-input {
    background: #fff;
  }
  &:focus,
  &:hover {
    border-color: #feb72b;
    box-shadow: 0 0 0 2px rgba(255, 152, 24, 0.08);
  }
`
